package com.example.prateek.springbatchcsvupload;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.data.builder.MongoItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableBatchProcessing
public class UserBatchJobConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    // @Autowired
    //private SimpleJobLauncher jobLauncher;

    @Bean
    public FlatFileItemReader<UserDto> reader() {
        return new FlatFileItemReaderBuilder<UserDto>().name("userItemReader")
                                                       .resource(new ClassPathResource("lakh-sample-data.csv")).delimited()
                                                       .names("email", "firstName", "lastName", "mobileNumber")
                                                       .fieldSetMapper(new BeanWrapperFieldSetMapper<UserDto>() {
                                                           {
                                                               setTargetType(UserDto.class);
                                                           }
                                                       }).build();
    }

    @Bean
    public MongoItemWriter<User> writer(MongoTemplate mongoTemplate) {
        return new MongoItemWriterBuilder<User>().template(mongoTemplate).collection("user")
                                                 .build();
    }

    @Bean
    public Job updateUserJob(JobCompletionNotificationListener listener, Step step1)
            throws Exception {
        return this.jobBuilderFactory.get("updateUserJob").incrementer(new RunIdIncrementer())
                                     .listener(listener).flow(step1).end().build();
    }

    @Bean
    public UserItemProcessor processor() {
        return new UserItemProcessor();
    }

    @Bean
    public Step step1(FlatFileItemReader<UserDto> itemReader, MongoItemWriter<User> itemWriter)
            throws Exception {
        return this.stepBuilderFactory.get("step1").<UserDto, User>chunk(5).reader(itemReader)
                                                                           .processor(processor()).writer(itemWriter).build();
    }

}
